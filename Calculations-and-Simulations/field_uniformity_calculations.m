## Basic Dimentioning Calculations

# Basic Dimentioning Calculations

## Constants
mu_0 = 1.25663706e-7; # m kg s-2 A-2 - the magnetic constant

## Our Desing Paramenters
side = 0.796; #  meters
turns_in_coil = 80; #Turns
current_in_coil = 1.25; # Amps
d = 0.5106e-3; # m (24AWG)
spacing1 = 0.4;


# For simulation
spacing_optimal =  side*0.5445; #  meters
spacing2 = 0.35;
spacing3 = 0.45;
spacing4 =0.50;
spacing5 =0.55;
spacing6 =0.60;
spacing7 =0.80;
spacing = [spacing2,spacing1,spacing_optimal,spacing3,spacing4,spacing5,spacing6,spacing7]';


## Calculations
gamma = spacing/side;
q = spacing;
a = side;
# optimal gamma = 0.5445 according to bibliography
x = linspace(-1,1,1000);
side_inch = 39.37* side;

#field_intensity_eqn_1 = 2*4.95e-5*turns_in_coil*current_in_coil/ (pi*side_inch/2) *2/((1+gamma^2)*sqrt(2+gamma^2))*10000;
#field_intensity_eqn_2 = 100000 * mu_0*current_in_coil*turns_in_coil*a^2 / (2*pi) * ( 2 / ( (a^2/4 + q.^2/4) * sqrt(a^2/2 +q.^2/4))); # Gauss
field_intensity = 100000 * mu_0*current_in_coil*turns_in_coil*a^2 ./ (2*pi) * ( 1 ./ ( (a^2/4 + (x + q./2).^2) .* sqrt(a^2/2 + (x + q./2).^2)) + 1 ./ ( (a^2/4 + (x - q./2).^2) .* sqrt(a^2/2 + (x - q./2).^2))); # Gauss

## Plots
figure(1);
plot(x,field_intensity(1,:));
hold on
plot(x,field_intensity(2,:));
plot(x,field_intensity(3,:));
plot(x,field_intensity(4,:));
plot(x,field_intensity(5,:));
plot(x,field_intensity(6,:));
plot(x,field_intensity(7,:));
plot(x,field_intensity(8,:));
legend("spacing = 0.35","spacing = 0.4","spacing = side*0.5445 = 0.4334","spacing = 0.45","spacing = 0.50","spacing = 0.55","spacing = 0.60","spacing = 0.80")
grid on
xlabel('x(m)')
ylabel('B(Gauss)')
title("Magnetic field strength along the axis of a “thin” square Helmholtz coil")

txt = ['Side of the coil:',num2str(side),'m'];
text(-0.7, 2.4, txt,'FontSize',16)


txt = ['Turns in coil:',num2str(turns_in_coil),];
text(-0.7, 2.3, txt,'FontSize',16)

txt = ['Current in coil:',num2str(current_in_coil),'A'];
text(-0.7, 2.2, txt,'FontSize',16)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[V,R] = voltage_calc(turns_in_coil,a,d,current_in_coil);
txt = ['Resistance in Coil:',num2str(R),'Ohm'];
text(-0.7, 2.1, txt,'FontSize',16)

txt = ['Required Voltage:',num2str(V),'V'];
text(-0.7, 2.0, txt,'FontSize',16)
