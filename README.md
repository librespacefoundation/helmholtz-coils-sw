# Helmholtz Cage Software

This repository includes all needed firmware, simulations, calculations, calibration, Python code, and documentation of Helmholtz cage set-up for testing ADCS Cubesat sub-system.

This project was developed in [hackerspace.gr](https://www.hackerspace.gr/) by [Libre Space Foundation](https://libre.space/).

## Use the Helmholtz Cage setup

To use the setup available in [hackerspace.gr](https://www.hackerspace.gr/) one should follow the steps outlined below:

1. Install dependancies for Python

```
pip install skyfield igrf numpy pyserial tensorflow

```
2. Connect the your Laptop to the Control Box USB port for data transferring and powering up the STM32 microcontroller
3. Plug in the external power source box
3. (Optional) If you want to perform a calibration procedure or make any changes to the source code, clone this [repository](https://gitlab.com/librespacefoundation/helmholtz-coils-sw) and flush the microcontroller with the code that can be found in `firmware` folder using [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html).
 > To perform the calibration consult the next section
4. If not already done, clone this [repository](https://gitlab.com/librespacefoundation/helmholtz-coils-sw), navigate to the folder `UART-communicator-Python` and perform the following:  
    * Change the TLE.txt file, according to your setup
    * Change the time.txt (This file denotes the start time of propagation
    * You can also pass parameters, such as seconds to propagate (-sec2prop), step (-step_sec), etc. (For more information consult `run_sim.py`)
    * Run `run_sim.py`
    * (Optional) record the error between the desired and the detected magnetic field, by running `accuracy_test.py`




## Calibrate the Helmholtz Cage setup

Performing the calibration procedure is recommended in the following situations:

- If the setup has been relocated or reassembled.
- If the magnetometer has been repositioned.
- If there is uncertainty about any of these factors.
- If you desire higher accuracy in your magnetic field simulation. To check the current accuracy information, please refer to [this link](docs/Magnetic-field-study-and-calculation.md).

In order to perform the calibration:

1. In firmware/Core/Src/main.c define `DO_CALIBRATION` as `true`, and flush the microcontroller using [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html). You can change the step. 
> Please be aware that the calibration process is time-consuming. You can find instructions on estimating the runtime [here](docs/Cage-calibration.md). During the code execution, the console displays information about the progress of the calibration, allowing you to estimate the waiting time.

2. Navigate to the `Helmholtz-Cage-Calibration` folder
3. Access the serial port and write the data to a file called 'raw_calibration_data.csv'
For example you could use `picocom`:
```
picocom -b 115200 /dev/ttyACM0 -g raw_calibration_data.csv
```
4. Run `run_data_reduction.m`, using Octave
5. Run `NN_regression.ipynb` to update the `model.json` and `model.h5` files


## Documentation

One can access additional information regarding the project's development here: [Software Development Documentation](docs/home.md).


## Contribute

The main repository lives on [Gitlab](https://gitlab.com/librespacefoundation/helmholtz-coils-sw) and all Merge Request should happen there.

## License

Licensed under the [GNU GENERAL PUBLIC LICENSE v3](LICENSE).
