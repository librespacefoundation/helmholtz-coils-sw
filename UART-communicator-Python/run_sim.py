from skyfield.api import EarthSatellite, load, wgs84
import igrf
from datetime import timedelta
import argparse
import csv
import numpy as np
import serial
import tensorflow as tf
import time

# igrf.base.build() #Run if the error "FileNotFoundError: [Errno 2] No such file or directory: ../igrf/src/igrf/igrf13_driver" occurs (as referred here https://github.com/space-physics/igrf/issues/13)

class MagneticFieldSimulator:

	def __init__(self):
		self.earths_magetic_field = [1.40553665, 28.6010971,37.3134041] # Set earth's magnetic field in your location and magnetometer orientation 
		self.duty_cycle_array = np.empty((0, 6), dtype=np.float64)
		self.mag_field_array = np.empty((0, 3), dtype=np.float64)
		self.parser = argparse.ArgumentParser()
		self._initialize_arguments()
		self.args = self.parser.parse_args()
		self.load_tle()
		self.init_time()
		self.provide_simulation_info()
		self.initialise_model()

	def _initialize_arguments(self):

		self.parser.add_argument('-initial_time', default='Software/UART-communicator-Python/time.txt')
		self.parser.add_argument('-sec2prop', default=500, type=int)
		self.parser.add_argument('-step_sec', default=20, type=float)
		self.parser.add_argument('-tofile', default=False, type=bool)
		self.parser.add_argument('-tle', default='Software/UART-communicator-Python/TLE.txt')
		self.parser.add_argument('-sat_name', default='PHASMA-GNT')
		self.parser.add_argument('-json_file', default='Software/Helmholtz-Cage-Calibration/model.json')
		self.parser.add_argument('-weights', default='Software/Helmholtz-Cage-Calibration/model.h5')
		self.parser.add_argument('-ser_port', default='/dev/ttyACM0')
		self.parser.add_argument('-ser_baud', default=115200, type=int)

	def load_tle(self):
		with open(self.args.tle, 'r') as file:
			lines = file.readlines()
		self.line1 = lines[0].strip()
		self.line2 = lines[1].strip()

	def init_time(self):
		with open(self.args.initial_time, 'r') as file:
			line = file.readline().strip()
		self.date = [int(part) for part in line.split(',') if part.strip().isdigit()]

		self.ts = load.timescale()
		self.t = self.ts.utc(year=self.date[0], month=self.date[1], day=self.date[2], 
		                     hour=self.date[3], minute=self.date[4], second=self.date[5])
		self.time_at_end = self.t + timedelta(seconds=self.args.sec2prop)

	def provide_simulation_info(self):
		self.satellite = EarthSatellite(self.line1, self.line2, self.args.sat_name, self.ts)
		print(f"--------------------------------------------")
		print(f"Generating magnetic field for {self.satellite.name}")
		print(f"Starting propagation from {self.satellite.epoch.utc_iso()}")
		print(f"Ending propagation at {self.time_at_end.utc_iso()}")
		print(f"--------------------------------------------")

	def initialise_model(self):
		self.duty_cycle_array = np.empty((0, 6), dtype=np.float64)
		with open(self.args.json_file, 'r') as json_file:
			loaded_model_json = json_file.read()
		self.loaded_model = tf.keras.models.model_from_json(loaded_model_json)
		self.loaded_model.load_weights(self.args.weights)

	def mag2csv(self):
		with open('tle2mag.csv', 'w', newline='') as csvfile:
			fieldnames = ['Date', 'Latitude', 'Longitude', 'Height', 'North', 'East', 'Down', 'Total', 'Inclination', 'Declination']
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()

			for _ in range(self.args.sec2prop):
				self.t += timedelta(seconds=self.args.step_sec)
				geocentric = self.satellite.at(self.t)
				lat, lon = wgs84.latlon_of(geocentric)
				height = wgs84.height_of(geocentric)
				formatted_date = f'{self.t.utc.year}-{self.t.utc.month:02d}-{self.t.utc.day:02d}'
				mag = igrf.igrf(formatted_date, glat=lat.degrees, glon=lon.degrees, alt_km=height.km)
				north = mag['north'].values[0]
				east = mag['east'].values[0]
				down = mag['down'].values[0]
				total = mag['total'].values[0]
				incl = mag['incl'].values[0]
				decl = mag['decl'].values[0]
				writer.writerow({
					'Date': self.t.utc_strftime('%Y %b %d %H:%M:%S'),
					'Latitude': lat.degrees,
					'Longitude': lon.degrees,
					'Height': height.km,
					'North': north,
					'East': east,
					'Down': down,
					'Total': total,
					'Inclination': incl,
					'Declination': decl
				})

	def TLE2duty(self):
		for _ in range(0, self.args.sec2prop, self.args.step_sec):
			geocentric = self.satellite.at(self.t)
			lat, lon = wgs84.latlon_of(geocentric)
			height = wgs84.height_of(geocentric)
			formatted_date = f'{self.t.utc.year}-{self.t.utc.month:02d}-{self.t.utc.day:02d}'
			mag = igrf.igrf(formatted_date, glat=lat.degrees, glon=lon.degrees, alt_km=height.km)
			north = mag['north'].values[0]
			east = mag['east'].values[0]
			down = mag['down'].values[0]
			mag_field = [north/1000, east/1000, down/1000]
			self.mag_field_array = np.vstack((self.mag_field_array, mag_field))
			self.t += timedelta(seconds=self.args.step_sec)
			
		self.duty_cycle_array = self.loaded_model.predict(np.array(self.mag_field_array))


	def normalization(self):
		self.norm_duty_cycle_array = np.empty((0, 6), dtype=np.float64)
		for row in range(self.duty_cycle_array.shape[0]):
			norm_duty_cycle_array_row = self.duty_cycle_array[row][:]
			
			
			if (self.earths_magetic_field[2] - self.mag_field_array[row][2] < 0): #positive magnetic field generation
				norm_duty_cycle_array_row[1] = 1
			elif (self.earths_magetic_field[2] - self.mag_field_array[row][2] > 0):
				norm_duty_cycle_array_row[0] = 1
			else:
				norm_duty_cycle_array_row[0] = 1
				norm_duty_cycle_array_row[1] = 1
		
			if (self.earths_magetic_field[1] - self.mag_field_array[row][1] < 0):
				norm_duty_cycle_array_row[2] = 1
			elif (self.earths_magetic_field[1] - self.mag_field_array[row][1] > 0):
				norm_duty_cycle_array_row[3] = 1			
			else:
				norm_duty_cycle_array_row[2] = 1
				norm_duty_cycle_array_row[3] = 1
			
			if (self.earths_magetic_field[0] - self.mag_field_array[row][0] < 0):
				norm_duty_cycle_array_row[5] = 1
			elif (self.earths_magetic_field[0] - self.mag_field_array[row][0] > 0):	
				norm_duty_cycle_array_row[4] = 1
			else:
				norm_duty_cycle_array_row[4] = 1
				norm_duty_cycle_array_row[5] = 1
						
			self.norm_duty_cycle_array = np.vstack((self.norm_duty_cycle_array, norm_duty_cycle_array_row))
			#self.norm_duty_cycle_array = np.array([[1,0.088, 0.856, 1, 0.973, 1]])


	def UART_transmission(self):
		print(f"--------------------------------------------")
		print(f"The required calculations have been performed. Initiating the transmission of control inputs through UART to the STM32...")

		serial_port = serial.Serial(self.args.ser_port, 
			      baudrate=self.args.ser_baud, 
				  timeout=2,
				  parity=serial.PARITY_NONE,
				  stopbits=serial.STOPBITS_ONE,
				  bytesize=serial.EIGHTBITS,
				  xonxoff = True,
				  rtscts=False)

		serial_port_RX = serial.Serial('/dev/ttyACM0', 
			baudrate=self.args.ser_baud, 
			timeout=2,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			bytesize=serial.EIGHTBITS)

		try:
			while(True):
				for i in range(self.norm_duty_cycle_array.shape[0]):
					formatted_values = []
					for value in self.norm_duty_cycle_array[i]:
						formatted_values.append(f'{value:.2f}')
					values_string = ','.join(formatted_values)
					values_bytes = values_string.encode('utf-8')
					try:
						serial_port.write(values_bytes)
						Progress = i / self.norm_duty_cycle_array.shape[0] * 100
						print(f"Progress:{Progress/100:.2f}/1		Transmitted: {values_string}")
		
						data_in = ""
						data_in = serial_port_RX.read(size = 800).decode() # if the number of data is known, set timeout=None when defining the Serial class
						serial_port_RX.flushInput()
						print("Received mag field :  %s"%(data_in.strip()))
						print("Commanded mag field : %s %s %s"%(self.mag_field_array[i,0],self.mag_field_array[i,1],self.mag_field_array[i,2]))
						print("---------------------------")

					except KeyboardInterrupt: 
						pass
					time.sleep(0.5)
		finally:
				serial_port.close()
				print("Serial port closed.")

if __name__ == "__main__":
	simulator = MagneticFieldSimulator()
	if simulator.args.tofile:
		simulator.mag2csv()
		print("Finished!")
	else:
		simulator.TLE2duty()
		simulator.normalization()
		simulator.UART_transmission()
		print("Finished!")
		
