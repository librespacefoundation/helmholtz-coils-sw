
[TOC]

# Machine Learning Data Mapping

Our goal was to create a method that uses a machine learning algorithm for organizing the data obtained following Helmholtz Cage Calibration. Our aim is to establish a rapid and effective means of associating the magnetic field data with the actuation input, the duty cycle.

## KMeans Algorithm

Initially, the K-means clustering tec## Determining maximum allowable delay in CL

Here, we provide information regarding the minimum required magnetic field rate. Essentially, we are sharing details about our process for determining the maximum permissible delay in ms that can be utilized within the control loop. This assessment is crucial in determining the feasibility of incorporating all the collected data points into the control loop.hnique was utilized — an unsupervised machine learning method for identifying clusters within datasets. K-Means clustering, an unsupervised algorithm, partitions objects into clusters with shared similarities and distinct dissimilarities.

The variable 'K' represents cluster quantity, e.g., K = 50 implies fifty clusters. Optimal K is vital, impacting result quality.

Optimal K determination involves techniques like the elbow method. The elbow method runs K-means for varied K values, plotting sum of squares error against clusters. A point where error reduction is not significant forms an "elbow," indicating reasonable clusters. 

The K-Means Clustering Algorithm is presented below:

```mermaid
graph TD
  A[Find the value of K]
  B[Initialize centroids to K random points]
  C[Measure the distance of the first point from all the centroids]
  Interim[Assign the first point to the cluster with a centroid from which the distance is minimum]
  D[Repeat for every data point, so you form K initial clusters]
  E[For these clusters calculate the new geometrical centroid position]

  F[Measure the distance from the first point to the new centroids]
  G[Repeat for every data point]
  H[Calculate the new centroids/mean positions of the new clusters]
  I[Convergence]
  
  A --> B
  B --> C
  C --> Interim
  Interim --> D
  D --> E
  E --> F
  F --> G
  G --> H

  H -->|Centroid moves| F
  H -->|Centroid stops moving withing a threshold| I
```

For this analysis, the python package, “scikit-learn” was used, and specifically, a module named `sklearn.cluster`. This module gathers popular unsupervised clustering algorithms. We utilized the KMeans class. For more information, refer to the [documentation](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html#sklearn.cluster.KMeans).

Furthermore, an Elbow analysis was conducted, and the resulting graph is as follows:

<p align="center">
  <img src="uploads/ElbowPoint.png" width="400" />
</p>



The analysis of the elbow point indicated that the optimal number of clusters is 51. Nevertheless, with this cluster count, the maximum error reaches 70.12 uT. This value surpasses the acceptable threshold for magnetic field error, which stands at 10 uT. As a result, this level of error is intolerable within the given context.

The highest error observed for 40,000 data points (constituting 30.7% of the dataset) is 9.69 uT. This outcome falls short of being optimal, particularly when considering the presence of additional error sources that require consideration. Evidently, the KMeans clustering approach demonstrates suboptimal performance in addressing this specific challenge. As a result, the exploration of various regression techniques is deemed necessary to find a more effective solution.

### Alternative techniques

Several potential courses of action are available for consideration:

1. **Exploring Alternative Techniques:** One option is to explore different grouping techniques, potentially including various forms of regression analysis. This approach could provide insights into how the data can be organized differently, which might lead to better results without exceeding the error threshold and time consumption. It's important to acknowledge that this option could elongate the project's development timeline.

2. **Scanning all the data with Acceptable Delay technique:** Consider utilizing the provided data, factoring in the acceptable time required to scan all 130,126 data points within the control loop. This option minimizes potential error, yet it does come with the drawback of consuming the maximum time within the control loop. However, this approach could be viable, particularly when the rate of change in the magnetic field is small.  Meticulous calculations and thoughtful assessment should be conducted. It's important to note that this option could potentially restrict the feasibility of configuring setups for modeling rapidly fluctuating magnetic fields.

3. **Increasing Clusters:** Another approach involves increasing the number of clusters used in the analysis. By utilizing more clusters, there's a chance that the error could decrease to a level below the designated threshold. This option positions itself between the two previously discussed choices. It has the potential to yield improved results without causing a notable elongation of the project's development timeline. While the error level may not be minimal as seen in the second option, it remains acceptable. Additionally, the runtime consumption will be substantially reduced, although not to the extent observed in the first option. 

The third option, involving increasing clusters number, will be examined first. This evaluation encompasses an examination of error reduction and runtime cost. Balancing error reduction with computational gain is a key consideration.

#### Increasing Clusters technique

The highest error observed for 40,000 data points (constituting 30.7% of the dataset) is 9.69 uT. This outcome falled short of being optimal, particularly when considering the presence of additional error sources that required consideration. Evidently, the KMeans clustering approach demonstrated suboptimal performance in addressing this specific challenge. As a result, the exploration of various regression techniques was deemed necessary.

#### Scanning all the data with Acceptable Delay technique

##### Determining maximum allowable delay in CL

Here, we provide information regarding the minimum required magnetic field rate. Essentially, we are sharing details about our process for determining the maximum permissible delay in ms that can be utilized within the control loop. This assessment is crucial in determining the feasibility of incorporating all the collected data points into the control loop.

The magnetic field rate of change is presented in the figure below:

<p align="center">
  <img src="uploads/B-rate.png" width="400" />
</p>

This is the magnetic field in the body frame for the orbit of the PHASMA mission (see TLE [here](https://gitlab.com/librespacefoundation/phasma/phasma-org/-/issues/88)). An ideal control system was utilized, with a focus on quickly aligning the body frame with the LVLH (also known as the orbit) frame. As a result, the magnetic field depicted in the plot was expressed in the LVLH frame. The timestep over which the derivative was calculated was 1 s.

Here are some considerations regarding the 1s delay:


This was chosen because it is the timestep used in some controllers on-board the PHASMA ADCS. However, this is found in de-tumbling controller, where the derivative of the magnetic field in the body frame is large due to the fast rotation. It is actually much faster than the change of magnetic field due to orbital motion (in fact this is a basic assumption for the Bdot controller). We considered re-evaluating the 1-second period. Nevertheless, we made the decision to maintain a conservative approach and adhere to the maximum 1-second delay.

##### Measuring total data scanning run-time

Due to the fact that the K Means algorithm proved ineffective with our dataset and the viability of the multiple regression technique was still being examined, we proceeded to access the time required to scan all data points for the control loop. Our chosen strategy involved utilizing Python on a Lenovo ThinkPad X1 Carbon 6th generation, featuring an Intel® Core™ i7-8550U CPU operating at 1.80GHz. If the run-time exceeds 1000 ms, the viability of full data scanning technique was to be questioned.

A NumPy array of magnetic field data points, with dimensions 130126x3, was analyzed. The procedure involved computing the Euclidean distances between each row in the array, containing magnetic field values, and a reference vector. The calculation was performed using the `np.linalg.norm` function. Subsequently, the code used the `np.argmin` function to identify the index of the minimum distance.

Although this approach might have imposed a heavy demand on computational resources when dealing with such substantial datasets, the code's performance was optimized through the use of NumPy's vectorized operations, resulting in enhanced efficiency.

The code could be found at the path `Software/Helhmoltz-Cage-Calibration/TestRunTime.py`. Alternatively, you could directly access the code through this [link](https://gitlab.com/vi2000/helmholtz-coils-trajectory-simulator/-/commit/27ff8c68e75dcac229338d326a1cc8829a9697b6).

The execution outcomes were deemed satisfactory, with the code completing within a time frame of approximately 6 to 10 milliseconds.

However, after discussion we discovered that the logic behind this type of scanning has a flaw. More specifically, the flaw is based on the following statement: "It is possible that 2 vectors have different coefficients, but the same magnitude." This can result in finding one (if not multiple!) vectors with the same magnitude but with different coefficients and choosing a duty cycle that will produce a magnetic field of the same magnitude, but of different direction.Thus, the magnetic field's angles were not taken into consideration.

In the meantime, a Neural Network technique was developed. And it appeared that the utilization of a Neural Network regression technique held greater promise. This was primarily due to its consistent execution time of 60 ms within the control loop, albeit being 6 times longer than the time taken for data scanning. It's important to note that this execution time would inevitably increase as a result of not factoring in the angles.

Moreover, it's worth highlighting that the quantity of data points in the existing code was minimal. Consequently, if someone aimed to enhance the precision of their magnetic field imposition within the Helmholtz Cage setup, they would need to decrease the intervals of the imposed duty cycles during the calibration process. For instance, when using a duty cycle step of 0.01, the number of scanned values reached 8,242,408. This would increase the total amount of data 59 times. Given that simple search algorithms operated with an O(n) complexity, the execution time could potentially reach 353 ms at best. This calculation didn't even encompass the time required to account for the vector's angles.

In contrast, the Neural Network technique maintained an almost constant time consumption of around 60 ms in the control loop regardless of the data amount. This level of computational expense was plausible for our specific setup. Notably, this cost didn't escalate with the increase in data points. The factor that experienced an increase was the calibration time for the Helmholtz Cage, a process that was performed only once.

Furthermore, attempting to access intermediate values within the full data scanning technique would introduce further errors. Techniques like linear interpolation could be inaccurate due to the lack of linear independence of magnetic field vectors resulting from mechanical imperfections in the current setup. These errors could be mitigated by employing the Neural Network technique that used a multiple regression method to access the proper duty cycle that would result in the desired magnetic field.

Hence, we decided to move forward with the Neural Network approach.



#### Using a Neural Network technique


Here, the use of a deep neural network as a function approximator is investigated.


##### Architecture

A simple neural network using Keras and Tensorflow was constructed for the task. The network receives a 1x3 vector as input and outputs a 1x6 vector. There is a single hidden layer with 256 neurons and a tanh activation function and an output layer with 3 neurons and a linear activation function. This type of network has been shown to be a universal function approximator [1] and was found to work well for the present task.
Since input data have large variations, a normalization layer was constructed and testing-training sample data were normalized. The normalizer was adapted on the whole dataset (both testing and training data). Any magnetic field triad that is fed into the neural network must be normalized first. Regarding the targets (duty cycle values), no normalization was needed, since those range from 0 to 1.
The weights of the hidden layer were initialized with a He uniform initializer.

##### Dataset

The dataset consists of around 130000 samples. The dataset was produced by increasing each duty cycle component by 0.04 (in "duty cycle units") while keeping the other 2 components constant. For each duty cycle vector 3 magnetic field measurements were taken and their average was logged as the resulting magnetic field entry. This process was performed for both mgnetic field directions in each coil pair, with the duty cycle value of the "non-active" direction being 1 and the "active" direction being between 0 and 1. Overall, 2 duty cycle entries theoretically producing opposite magnetic field components would look like this :


```
0.16,1.0,1.0,0.24,0.40,1.0
1.0,0.16,0.24,1.0,1.0,0.40

```


##### Training

The network was trained on samples randomly chosen from the dataset and constituting 80% of it. Mean Squared Error (MSE) was used as the error metric, although Mean Absolute Error could have been used as well. Moreover a validation split of 20% and a batch size of 64 were chosen. Morerover an ADAM optimizer was chosen, with a learning rate of 0.001.
Training was conducted for 20 epochs, shuffling the data before each epoch, with the loss vs epoch plot presented below. Training is pretty fast (expected from such a small network), taking around 5s/epoch on an Intel i5-8350U CPU.


<p align="center">
  <img src="uploads/NN.png" width="400" />
</p>

##### Testing

Testing was conducted on the rest 20% of the dataset.

##### Results

Evaluating the model described above on the testing dataset gives a MAE of 0.0114 (in "duty cycle units"). The run time for a single prediction ranges form 70 to 100 ms, which probably includes some overhead in the prediction function because performing a set of 26000 predictions only takes 1.3 sec. In any case, even if the predictions are made one-by-one, the run time is acceptable.

##### Network tuning

Evaluating the model described above on the testing dataset gives a MAE of 0.0114 (in "duty cycle units"). The run time for a single prediction ranges form 70 to 100 ms, which probably includes some overhead in the prediction function because performing a set of 26000 predictions only takes 1.3 sec. In any case, even if the predictions are made one-by-one, the run time is acceptable.

##### Bibliography

Hagan, M.T., Demuth, H.B., Beale, M.H., De Jes{\'u}s, O. (2014). \textit{Neural Network Design}. Martin Hagan.


