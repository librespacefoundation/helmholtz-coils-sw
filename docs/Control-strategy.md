[TOC]

# Control strategy


# Initial approach

The initial concept envisioned entailed deriving the State space equation governing the current behavior in a pair of coils. These coils would have been modeled using a series arrangement of resistor and inductor elements, following an identical configuration for the second coil. The governing control input would have been the voltage, which would have been determined by establishing its correlation with the duty cycle through testing. In our scenario, this duty cycle would have served as the actual control input.

The relationship between current and magnetic field is linear, and it can be computed mathematically. This relationship could then have been validated for accuracy within the final setup, potentially involving minor parameter adjustments for precise tuning.The magnetic field would have formed the state vector, while feedback for the loop would have been provided by a magnetic field sensor. The potential implementation could have encompassed the integration of a PID or LQR controller.

However, an important revelation emerged from the testing phase. It was determined that our setup is not governed by any linear or established relationship due to the interdependence of currents, stemming from the shared power source, and the presence of mechanical imperfections. As a result, the creation of a state space equation that would replicate these intricate relationships is rendered unfeasible. As a consequence, the previously outlined controllers are rendered inapplicable.

# Final approach

Instead of proceeding with the previously outlined approach, in order to attain accurate outcomes, a calibrated approach becomes imperative. The calibration procedure involves logging magnetic field values along the x, y, and z axes, corresponding to different duty cycle settings. The goal is to record a wide range of potential discrete states of the system. This will be done by incrementally varying the duty cycle from 0% to 100%, using a specified step size. Importantly, reducing the step size will enhance the precision of the configuration, albeit at the cost of longer runtime.

The duration of the calibration process can vary, spanning from 30 minutes to 3 hours, contingent upon the desired precision level. Importantly, any alterations to the foundational setup or relocation of the equipment mandate a re-calibration procedure to ensure the precision of results in magnetic field imposition.

Thus, instead of proceeding with the previously outlined approach, after performing the calibration procedure, an open-loop control strategy will be implemented as follows: 

```mermaid
flowchart TD;
  A[Get desired magnetic field values];
  B[Get the corresponding duty cycle using a multiple regression neural network model];
  C[Transmit a value via UART to the STM32 micro-controller];
  D[Impose the magnetic field corresponding to this duty cycle];


  A --> B;
  B --> C;
  C --> D;
  D --> C;
```

# Further development suggestions

An ideal approach would involve implementing a model-based adaptive control strategy. Adaptive control algorithms dynamically adjust their parameters based on observed system behavior, effectively “learning” system dynamics and adapting to changes over time. Model-free adaptive control is useful when system modeling is uncertain or when characteristics change, negating the need for lengthy calibration procedures. This offers a promising direction for future development.

