
This page encompasses all the necessary links to documentation for the software development of the Helmholtz Cage, which is crucial for testing the ADCS (Attitude Determination and Control System) of the CubeSat sub-system.

* [Requirements](Requirements.md)
* [Magnetic field study and calculation](Magnetic-field-study-and-calculation.md)
* [Magnetometer Calibration](Magnetometer-Calibration.md)
* [Helmholtz Cage calibrbation procedure development](Cage-calibration.md)
* [Machine Learning Data Mapping](Machine-Learning-Data-Mapping.md)
* [Control strategy](Control-strategy.md)
* [Conversion of TLE to magnetic field](TLE2mag.md)





