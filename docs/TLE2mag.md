[TOC]



# Conversion of TLE to magnetic field

## TLE

Foremost, the location of an orbital object in space at a particular moment is stored in a **two-line element set (TLE)**. The state (position and velocity) at any time in the past or future can be roughly estimated using a suitable propagation method. For instance, we could use a simplified perturbations model SDP4.

As an illustration, a TLE for the International Space Station is provided as follows:

```
ISS (ZARYA)
1 25544U 98067A   04236.56031392  .00020137  00000-0  16538-3 0  9993
2 25544  51.6335 344.7760 0007976 126.2523 325.9359 15.70406856328906
```

| Columns | Example        | Description                                                                     |
|---------|----------------|---------------------------------------------------------------------------------|
| {+Line 0+}  |                |                                                                                 |
|   1-24  |   ISS (ZARYA)  | The common name for the object based on information from the Satellite Catalog. |
| {+Line 1+}  |                |                                                                                 |
|    1    |        1       |                                   Line Number                                   |
|   3-7   |      25544     |                             Satellite Catalog Number                            |
|    8    |        U       |                               Elset Classification                              |
|  10-17  |     98067A     |                             International Designator                            |
|  19-32  | 04236.56031392 |     Element Set Epoch (UTC) *Note: spaces are acceptable in columns 21 & 22     |
|  34-43  |    .00020137   |              1st Derivative of the Mean Motion with respect to Time             |
|  45-52  |     00000-0    |  2nd Derivative of the Mean Motion with respect to Time (decimal point assumed) |
|  54-61  |     16538-3    |                                   B* Drag Term                                  |
|    63   |        0       |                                 Element Set Type                                |
|  65-68  |       999      |                                  Element Number                                 |
|    69   |        3       |                                     Checksum                                    |
| {+Line 2+}  |                |                                                                                 |
|    1    |        2       | Line Number                                                                     |
|   3-7   |      25544     | Satellite Catalog Number                                                        |
|   9-16  |     51.6335    | Orbit Inclination (degrees)                                                     |
|  18-25  |    344.7760    | Right Ascension of Ascending Node (degrees)                                     |
|  27-33  |     0007976    | Eccentricity (decimal point assumed)                                            |
|  35-42  |    126.2523    | Argument of Perigee (degrees)                                                   |
|  44-51  |    325.9359    | Mean Anomaly (degrees)                                                          |
|  53-63  |   15.70406856  | Mean Motion (revolutions/day)                                                   |
|  64-68  |      32890     | Revolution Number at Epoch                                                      |
|    69   |        6       | Checksum                                                                        |

## Conversion Process
In this project, the `Skyfield` library is used. The package is built upon a Python SGP4 library, which compiles the official C++ code from the Revisiting Spacetrack Report #3 (AIAA 2006-6753) and uses the Simplified General Perturbations Model 4. This library is used for the computation of satellite positions within Earth's orbit at specific times. The propagation process relies on user-defined step and time values in seconds for propagation. Subsequently, upon acquiring the ephemeris (position and velocity over time), conversion into geographic coordinates (latitude, longitude, and height) is performed. This conversion is carried out using the `igrf` library, which employs the International Geomagnetic Reference Field Model for the computation of magnetic fields in the NED (north-east-down) coordinate system. The axes are depicted in the image provided below:



<p align="center">
  <img src="uploads/axes.png" width="400" />
</p>



Alternatively, if a different setup is in use, reference should be made to the [RM3100-Testing-Boards-User-Manual p.9](https://www.pnicorp.com/wp-content/uploads/RM3100-Testing-Boards-User-Manual-r04.pdf), and the axes should be defined based on the magnetometer's position in the calibration procedure.

Following the acquisition of the NED magnetic field, conversion is carried out using a neural network multiple regression model implemented through the TensorFlow Python package. This conversion transforms the desired magnetic field into duty cycle values, which serve as the control input for the H-bridges driving the coils. The resulting duty cycle is transmitted via UART to an STM32 development board, which in turn drives the H-bridges, reads the magnetic field through the magnetometer, and sends back the actual magnetic field data via UART.



