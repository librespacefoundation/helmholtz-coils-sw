#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "TLE.h"

int main(void)
{

char line1[] ="1 31792U 07029A   22289.01338767 -.00000054  00000+0 -35363-5 0  9993";
char line2[] ="2 31792  70.9514 286.9916 0011175 231.8951 128.1163 14.12601030788952";


TLE tle;
double r[3];
double v[3];
double mins = 1440;


parseLines(&tle,line1,line2);
getRV(&tle,mins,r,v);

printf("r: x = %lf,y = %lf,z = %lf km\n",r[0], r[1],r[2]);

printf("v: x = %lf,y = %lf,z = %lf km/s\n",v[0], v[1],v[2]);
    return 0;
}

error_status = init_tle();

update_sgp4()

sprintf(tle_string,
                "1 25544U 98067A   16229.19636472  .00005500  00000-0  87400-4 0  9991\n2 25544  51.6439 118.5889 0001926 134.0246   3.7037 15.55029964 14324");
temp_tle = read_tle(tle_string);
update_tle(&upsat_tle, temp_tle);

