/*
 * RM3100.h
 *
 *  Created on: May 2, 2023
 *      Author: Victoria Malyshkina
 */

#include "stm32l4xx_hal.h" /* Needed for I2C*/

#ifndef RM3100_H_
#define RM3100_H_

/*
 * DEFINES
 */

/*
 * REGISTERS
*/
#define PNI_POLL        0x00    // Polls for a Single Measurement
#define PNI_CMM         0x01    // Initiates Continuous Measurement Mode

#define PNI_CCX_MSB     0x04    // Cycle Count Register – X Axis
#define PNI_CCX_LSB     0x05    // Cycle Count Register – X Axis
#define PNI_CCY_MSB     0x06    // Cycle Count Register – Y Axis
#define PNI_CCY_LSB     0x07    // Cycle Count Register – Y Axis
#define PNI_CCZ_MSB     0x08    // Cycle Count Register – Z Axis
#define PNI_CCZ_LSB     0x09    // Cycle Count Register – Z Axis

#define PNI_TMRC        0x0B    // Sets Continuous Measurement Mode Data Rate
#define PNI_ALLX        0x0C    // Alarm Lower Limit – X Axis
#define PNI_AULX        0x0F    // Alarm Upper Limit – X Axis
#define PNI_ALLY        0x12    // Alarm Lower Limit – Y Axis
#define PNI_AULY        0x15    // Alarm Upper Limit – Y Axis
#define PNI_ALLZ        0x18    // Alarm Lower Limit – Z Axis
#define PNI_AULZ        0x1B    // Alarm Upper Limit – Z Axis
#define PNI_ADLX        0x1E    // Alarm Hysteresis Value – X Axis
#define PNI_ADLY        0x20    // Alarm Hysteresis Value – Y Axis
#define PNI_ADLZ        0x22    // Alarm Hysteresis Value – Z Axis

#define PNI_MX          0x24    // Measurement Results – X Axis
#define PNI_MY          0x27    // Measurement Results – Y Axis
#define PNI_MZ          0x2A    // Measurement Results – Z Axis

#define PNI_BIST        0x33    // Built-In Self Test
#define PNI_STATUS      0x34    // Status of DRDY
#define PNI_HSHAKE      0x35    // Handshake Register
#define PNI_REVID       0x36    // MagI2C Revision Identification

#define SM_ALL_AXIS     0x70    // Single measurement mode
#define STATUS_MASK     0x80    // To get status of data ready
//#define PNI_CyclesMSB   0x00
//#define PNI_CyclesLSB   0xC8
#define PNI_GAIN        1e3/75.0/1000  // uT/LSB
#define PNI_DEFAULT_ID  (0x20<<1)   // Hexadecimal slave address for RM3100
#define PNI_TIMEOUT     500       // in ms


#define A_MGN  0.7    // Coefficient < 1.0

typedef struct {
	/*I2C handle*/
	I2C_HandleTypeDef *i2cHandle;
	/* Magnetometer data (X,Y,Z) in uT  */
	float mag_rm3100[3];
}RM3100;

/*
 * INITIALIZATION
*/
uint8_t RM3100_Initialise(RM3100 *dev, I2C_HandleTypeDef *i2cHandle);

/*
 * DATA ACQUISITION
*/
HAL_StatusTypeDef RM3100_ReadMagneticField(RM3100 *dev);

/*
 * LOW-LEVEL FUNCTIONS
*/
HAL_StatusTypeDef RM3100_Read_Register(RM3100 *dev, uint8_t reg, uint8_t *data);
HAL_StatusTypeDef RM3100_Read_Registers(RM3100 *dev, uint8_t reg, uint8_t *data,uint8_t length);
HAL_StatusTypeDef RM3100_Write_Register(RM3100 *dev, uint8_t reg, uint8_t *data);


#endif /* RM3100_H_ */
